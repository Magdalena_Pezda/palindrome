//============================================================================
// Name        : Palindrome.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

bool isPalindrome (string &word);

int main() {

	string word;
	cout<<"Enter word";
	cin>>word;

	if (isPalindrome(word)) {
		cout << "It is palindrome";
	}
	else
		cout<<"It isn't palindrome";

}



bool isPalindrome (string &word) {
	const char *myWord = word.c_str();
	int size = word.size();

	const char *ptr = myWord;
	const char *ptr2 = &myWord[size-1];

	do {
		if (*ptr == *ptr2) {
			 ptr++;
			 ptr2--;
		}
		else
			return 0;
	} while (*ptr != *ptr2);
	return 1;
}


